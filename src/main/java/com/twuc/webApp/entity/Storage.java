package com.twuc.webApp.entity;


import sun.awt.util.IdentityLinkedList;

import java.util.HashMap;
import java.util.Map;

public class Storage {
    private Map<Ticket,Bag> storage = new HashMap<>();;
    private int big;
    private int medium;
    private int small;

    public Storage(int big) {
        this.big = big;
    }

    public Storage(int big, int medium, int small) {
        this.big = big;
        this.medium = medium;
        this.small = small;
    }

    public Ticket save(Bag bag){
        if (storage.size()<big+medium+small){
            Ticket ticket = new Ticket();
            storage.put(ticket,bag);
            return ticket;
        }
        else
            throw new IllegalArgumentException("Insufficient capacity");
    }

    public Ticket save(Bag bag,int size) {
        if (size == 0) {
            if (bag.getSize()==0&&small > 0) {
                Ticket ticket = new Ticket();
                storage.put(ticket, bag);
                return ticket;
            }
            else throw new IllegalArgumentException("Insufficient capacity");
        }
        else if(size == 1) {
            if (bag.getSize() <= 1 && medium > 0) {
                Ticket ticket = new Ticket();
                storage.put(ticket, bag);
                return ticket;
            }
            else throw new IllegalArgumentException("Insufficient capacity");
        }else {
            if(bag.getSize()<=2&&big>0){
                Ticket ticket = new Ticket();
                storage.put(ticket, bag);
                return ticket;
            }
            else throw new IllegalArgumentException("Insufficient capacity");
        }


    }

    public Bag retrieve(Ticket ticket) {
        if(!storage.containsKey(ticket)){
            throw new IllegalArgumentException("Invalid Ticket");
        }
        else{
            return storage.remove(ticket);
        }
    }
}
