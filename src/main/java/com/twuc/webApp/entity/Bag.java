package com.twuc.webApp.entity;

public class Bag {
    private int size;

    public Bag() {
    }

    public Bag(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
