import com.twuc.webApp.entity.Bag;
import com.twuc.webApp.entity.Storage;
import com.twuc.webApp.entity.Ticket;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class test {
    private Storage createStorageWithCapacity(){
        return new Storage(10,10,10);
    }

    @Test
    void should_get_ticket_when_saving_bag() {
        //given
        Storage storage = createStorageWithCapacity();
        Bag bag = new Bag();
        //when
        Ticket ticket =  storage.save(bag);
        //then
        assertNotNull(ticket);
    }

    @Test
    void should_get_ticket_when_save_nothing() {

        Storage storage = createStorageWithCapacity();
        final Bag nothing = null;

        Ticket ticket = storage.save(nothing);
        assertNotNull(ticket);
    }

    @Test
    void should_get_saved_bag_when_retrieving_with_valid_ticket() {
        Bag expectedBag = new Bag();
        Storage storage = createStorageWithCapacity();
        Bag anotherBag = new Bag();
        storage.save(anotherBag);

        Ticket validTicket = storage.save(expectedBag);

        Bag retrieve = storage.retrieve(validTicket);

        assertSame(expectedBag,retrieve);
        assertNotSame(anotherBag,retrieve);
    }

    @Test
    void should_get_error_message_when_retrieving_with_invalid_ticket() {
        Storage storage = createStorageWithCapacity();
        Ticket invalidTicket = new Ticket();

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> storage.retrieve((invalidTicket)));

        assertEquals("Invalid Ticket",exception.getMessage());
    }

    @Test
    void should_get_nothing_when_save_nothing() {
        Storage storage = createStorageWithCapacity();
        Ticket ticket = storage.save(null);

        Bag retrieve = storage.retrieve(ticket);

        assertNull(retrieve);
    }

    @Test
    void should_get_ticket_when_save_into_empty_storage() {
        Storage  emptyStorage = new Storage(2);
        Bag bag = new Bag();

        Ticket ticket = emptyStorage.save(bag);

        assertNotNull(ticket);
    }

    @Test
    void should_save2_bags_when_capacity_is1() {
        Storage storage = new Storage(1);

        Ticket ticket = storage.save(new Bag());

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> storage.save(new Bag()));

        assertNotNull(ticket);
        assertEquals("Insufficient capacity",illegalArgumentException.getMessage());
    }

    @Test
    void should_not_save_when_storage_is_full() {
        Storage storage = new Storage(1);
        storage.save(new Bag());

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> storage.save(new Bag()));

        assertEquals("Insufficient capacity",illegalArgumentException.getMessage());
    }


    @Test
    void should_get_ticket_when_save_to_slot() {
        Storage storage = new Storage(0, 1, 0);

        Bag smallBag = new Bag(0);

        Ticket ticket = storage.save(smallBag, 1);

        assertNotNull(ticket);
    }
}
